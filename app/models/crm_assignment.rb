class CrmAssignment < ActiveRecord::Base

	has_many 	:crm_meetings
	belongs_to 	:crm_issue, :class_name => "CrmIssue", :foreign_key => "crm_issue_id"
	belongs_to 	:citizen, :class_name => "Citizen", :foreign_key => "citizen_id"

	

end
