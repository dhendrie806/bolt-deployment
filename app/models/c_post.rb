class CPost < ActiveRecord::Base

	belongs_to :citizen , :class_name => "Citizen", :foreign_key => "citizen_id"
	belongs_to :area , :class_name => "Area", :foreign_key => "area_id"
	belongs_to :organisation , :class_name => "Organisation", :foreign_key => "organisation_id"
	
end
