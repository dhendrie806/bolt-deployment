class CContact < ActiveRecord::Base

	belongs_to :citizen , :class_name => "Citizen", :foreign_key => "citizen_id"


	scope :sorted , lambda {order("c_contacts.created_at DESC")}
	scope :public , lambda {where(:public => true)}
	scope :notpublic , lambda {where(:public => false)}

end
