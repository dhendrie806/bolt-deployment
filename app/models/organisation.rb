class Organisation < ActiveRecord::Base

	has_many 	:citizens 
	has_many	:o_contacts
	has_many	:crm_issues
	has_many	:c_posts

	has_many :crm_assignments, :through => :crm_issue
	scope :sorted , lambda {order("organisations.name ASC")}
	scope :clients , lambda {where(:client => true)}

	has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "100x100>", :smallthumb => "50x50>" },:default_url => "missing_org_:style.png"
	validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/


	def self.import(file)
		CSV.foreach(file.path, headers: true) do |row|
			Organisation.create! row.to_hash
		end 
	end 


end
