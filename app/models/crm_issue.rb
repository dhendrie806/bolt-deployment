class CrmIssue < ActiveRecord::Base

	has_many 	:crm_assignments
	belongs_to	:organisation, :class_name => "Organisation", :foreign_key => "organisation_id"
	has_many 	:crm_meetings, :through => :crm_assignments
	has_many 	:citizens, :through => :crm_assignment


end
