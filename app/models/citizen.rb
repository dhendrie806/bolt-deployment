class Citizen < ActiveRecord::Base

	include PublicActivity::Model 
	tracked

	belongs_to :organisation , :class_name => "Organisation", :foreign_key => "organisation_id"
	has_many :c_contacts
	has_many :c_posts
	has_many :c_infos
	has_many :c_profiles
	has_many :crm_assignments
	has_many :crm_meetings, :through => :crm_assignments
	has_many :crm_issues, :through => :crm_assignments

	scope :sorted , lambda {order("citizens.last_name ASC")}

	has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "100x100>", :smallthumb => "50x50>" },:default_url => "missing_:style.png" 

	validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/

def fullname 
 	"#{self.first_name} #{self.last_name}"
 end 

 def self.items 
 	[1,2,3,4,5,6,7,8,9]
end 
end
