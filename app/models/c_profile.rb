class CProfile < ActiveRecord::Base

	belongs_to :citizen , :class_name => "Citizen", :foreign_key => "citizen_id"

	scope :short_profile, lambda { where(profile_type: "short") }
	scope :long_profile, lambda { where(profile_type: "long") }
	scope :condensed_profile, lambda { where(profile_type: "condensed") }


end
