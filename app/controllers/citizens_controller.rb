class CitizensController < ApplicationController


  def index

    @citizens = Citizen.sorted
    @Organisation = Organisation.sorted

  end

  def show

    @citizen = Citizen.find(params[:id])
    @contacts = @citizen.c_contacts.sorted
    @shortprofile = @citizen.c_profiles
    @recentmeetings = @citizen.crm_meetings.order('date DESC').limit(7)
    @meetings = @citizen.crm_meetings

  end

  def new
    @citizen = Citizen.new
    @defaultorg = 1
  end

   def create 

    @citizen = Citizen.new(params.require(:citizen).permit(:organisation_id, :first_name, :last_name, :prefix, :notes, :dob,:avatar,))

    if @citizen.save

      redirect_to(:action => 'index')

    else

      render('new')
    end 
 end    

  def edit

    @citizen = Citizen.find(params[:id])

  end

  def update
    #find exsisting object using params
    @citizen = Citizen.find(params[:id])
    if @citizen.update_attributes(params.require(:citizen).permit(:organisation_id, :first_name, :last_name, :prefix, :notes, :dob,:avatar,))

      redirect_to(:action => 'show', :id => @citizen.id)

    else

      render('edit')
    end 
  end 

  def delete
  end

  def report_long

    render :layout => false

    @citizen = Citizen.find(params[:id])


  end

  def self.updateimage

    @citizens = Citizen.all

    @citizens.each do |f|

       f.update_attribute :avatar ,File.new("/media/lucidus/F drive1/"+f.image, "rb")

     end 
   end 

     def self.deporgs

    dorgs = []

     Organisation.all.each do |o|

      if o.citizens.count >= 1

        puts o.name

        dorgs.push o

      end 

    end 

      return dorgs

  end 

   def mlaindex

    @citizen = Citizen.all.limit(10)
    @organisation = Organisation.where(org_type: "Political Party")

    #CitizensController.deporgs

  end 




end
