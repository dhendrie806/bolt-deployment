class CrmMeetingsController < ApplicationController
  def index

    @crmmeeting = CrmMeeting.find(params[:id])

  end

  def show
  end

  def new



  end

  def create
        @crmmeeting = CrmMeeting.new(params.require(:crm_meeting).permit(:crm_assignment_id, :date, :determination_id, :printout, :notes,))

    if @crmmeeting.save

      @crmmeeting.crm_assignment.update_column(:recent_determination, @crmmeeting.crm_assignment.crm_meetings.order(date: :desc).first.crm_determination.determination)
      @crmmeeting.crm_assignment.update_column(:recent_determination_date, @crmmeeting.crm_assignment.crm_meetings.order(date: :desc).first.date)
      flash[:success] = "Meeting added succesfully"

      redirect_to "/citizens/show?id="+@crmmeeting.crm_assignment.citizen.id.to_s


    else

      render('new')

    end 
    ###### loop determination assignment #####
#      @assignmentloop = CrmAssignment.all
 #    @assignmentloop.each do |f|
  #      f.update_column(:recent_determination, f.crm_meetings.order(date: :desc).first.crm_determination.determination)
   #     f.update_column(:recent_determination_date, f.crm_meetings.order(date: :desc).first.date)
    #  end 
     
  end 

  def edit
  end

  def delete
  end

  def cnewmeeting

   @crmmeeting = CrmAssignment.find(params[:id]).crm_meetings.new
    @crmissue = CrmAssignment.find(params[:id]).id
    @crmcitizen = CrmAssignment.find(params[:id]).citizen.fullname
end
end
