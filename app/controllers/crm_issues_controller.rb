class CrmIssuesController < ApplicationController
  def index

  	@issues = CrmIssue.all

  end

  def show
  	@issue = CrmIssue.find(params[:id])
  end

  def new 
    @crmissue = CrmIssue.new 
    @status = ["On-going", "Closed"]

  end 

  def create

        @citizen = CrmIssue.new(params.require(:crm_issue).permit(:organisation_id, :name, :status,))

    if @citizen.save

      redirect_to(:action => 'index')

  end 
  end 
  def edit
  end

  def delete
  end
end
