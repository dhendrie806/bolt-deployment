class OrganisationsController < ApplicationController


  def index

    @organisations = Organisation.sorted

  end

  def show

      @organisation = Organisation.find(params[:id])

  end

  def new
    @organisation = Organisation.new 
  end

  def create
    @organisation = Organisation.new(params.require(:organisation).permit(:name, :short_name, :org_type, :abbreviation, :notes, :extinct,:avatar, :client, :founded))

    if @organisation.save

      redirect_to(:action => 'show', :id => @organisation.id)

    else

      render('new')
    end 
  end

  def edit

    @organisation = Organisation.find(params[:id])

  end

  def update
    #find exsisting object using params
    @organisation = Organisation.find(params[:id])
    if @organisation.update_attributes(params.require(:organisation).permit(:name, :short_name, :org_type, :abbreviation, :notes, :extinct,:avatar, :client, :founded))

      redirect_to(:action => 'show', :id => @organisation.id) 
      flash[:success] = "You have successfully Updated the record"

    else

      render('edit')
    end 


  end 

  def delete
  end

  def select_citizens

    @sc = Citizen.all
    @organisation = Organisation.find(params[:id])

  end 

  def add_citizens 

    @organisation = Organisation.find(params[:organisation][:id])
    @selected = params[:citizen_ids]



     @newcposts = []

     @selected.each do |a|

       @newcposts.push CPost.new(:organisation_id  => @organisation.id, :citizen_id => a)

     end 

     @newcposts.each do |cpost|

      cpost.save
      flash[:success] = "You succesfully added "+ @newcposts.count.to_s + " new staff or members to this organisation"
    end  

    redirect_to(:action => 'show', :id => @organisation.id)
  end 


  def import
    

    Organisation.import(params[:file])
    flash[:success] = "You succesfully imported your organisations from CSV"
    redirect_to(:action => 'index')

  end 

  def selectimport

  end 
end
