class AddDateToAssignment < ActiveRecord::Migration
  def change
  	add_column :crm_assignments, :recent_determination_date, :date
  end
end
