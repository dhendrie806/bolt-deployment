class CreateCitizens < ActiveRecord::Migration
  def change
    create_table :citizens do |t|

    	t.integer 	"organisation_id"
    	t.string 	"first_name"
    	t.string 	"last_name"
    	t.string 	"prefix"
    	t.string 	"suffix"
    	t.string 	"salutation"
    	t.string 	"notes"
    	t.date   	"dob"
    	t.boolean	"extinct"
    	t.string	"profile_title"
		t.timestamps

    end
    add_index("citizens", "organisation_id")
  end
end
