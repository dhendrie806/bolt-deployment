class CreateOrganisations < ActiveRecord::Migration
  def change
    create_table :organisations do |t|

    	t.string 	"name"
    	t.string 	"short_name"
    	t.string	"org_type"
    	t.string 	"abbreviation"
    	t.boolean	"extinct"
    	t.string	"notes"
    	t.boolean	"client"
    	t.date		"founded"
     	t.timestamps
     	
    end
  end
end
