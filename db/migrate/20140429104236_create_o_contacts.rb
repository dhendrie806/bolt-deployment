class CreateOContacts < ActiveRecord::Migration
  def change
    create_table :o_contacts do |t|

	t.integer	"organisation_id"
    t.string	"contact_type"
    t.string	"location"
    t.string	"address_one"
    t.string	"address_two"
    t.string	"address_three"
    t.string	"city"
    t.string	"postcode"
    t.string	"telephone"
    t.string	"email"
    t.string	"website"
    t.boolean	"public"
    t.timestamps	

    end

    add_index("o_contacts", "organisation_id" )

  end
end
