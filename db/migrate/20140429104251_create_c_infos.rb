class CreateCInfos < ActiveRecord::Migration
  def change
    create_table :c_infos do |t|

    	t.integer	"citizen_id"
    	t.string	"info_type"
    	t.string	"info"
    	t.boolean	"printout"
    	t.timestamps

    end

    	add_index("c_infos","citizen_id")

  end
end
