class CreateCPosts < ActiveRecord::Migration
  def change
    create_table :c_posts do |t|

    	t.integer	"citizen_id"
    	t.integer	"organisation_id"
    	t.integer	"area_id"
    	t.date		"start_date"
    	t.date      "end_date"
    	t.boolean	"current"
    	t.string	"title"
    	t.string	"role"
    	t.string	"description"
    	t.string	"post_type"
    	t.boolean	"political"
    	t.boolean	"printout"
    	t.boolean	"keyrole"
		t.timestamps

    end

    	add_index("c_posts", "organisation_id")
    	add_index("c_posts", "citizen_id")
    	add_index("c_posts", "area_id")

  end
end
