class CreateCProfiles < ActiveRecord::Migration
  def change
    create_table :c_profiles do |t|

    	t.integer	"citizen_id"
    	t.string	"profile_type"
    	t.string	"profile"
    	t.timestamps

    end

    	add_index("c_profiles","citizen_id")

  end
end
