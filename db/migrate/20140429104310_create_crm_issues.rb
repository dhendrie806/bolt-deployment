class CreateCrmIssues < ActiveRecord::Migration
  def change
    create_table :crm_issues do |t|

    	t.integer 	"organisation_id"
    	t.string	"name"
    	t.string	"status"
    	t.timestamps
    	
    end

    	add_index("crm_issues","organisation_id")

  end
end
