class CreateCrmMeetings < ActiveRecord::Migration
  def change
    create_table :crm_meetings do |t|

    	t.integer	"crm_assignment_id"
    	t.date		"date"
    	t.string	"notes"
    	t.string    "determination"
    	t.boolean	"printout"
    	t.string	"event"
        t.timestamps

    end

    add_index("crm_meetings","crm_assignment_id")

  end
end
