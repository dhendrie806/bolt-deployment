class CreateAreas < ActiveRecord::Migration
  def change
    create_table :areas do |t|

    	t.string	"area_type"
    	t.string	"name"
    	t.timestamps
    	
    end
  end
end
