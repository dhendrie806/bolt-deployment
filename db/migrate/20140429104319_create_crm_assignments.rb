class CreateCrmAssignments < ActiveRecord::Migration
  def change
  		    create_table :crm_meetings do |t|

    	t.integer	"crm_issue_id"
    	t.integer	"citizen_id"
    	t.boolean	"noprevious"
    	t.boolean    "printout"
        t.timestamps
	end  


    	add_index("crm_assignments","crm_issue_id")
    	add_index("crm_assignments","citizen_id")
    	
  end
end
