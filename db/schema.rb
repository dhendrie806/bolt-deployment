# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140610112016) do

  create_table "activities", force: true do |t|
    t.integer  "trackable_id"
    t.string   "trackable_type"
    t.integer  "owner_id"
    t.string   "owner_type"
    t.string   "key"
    t.text     "parameters"
    t.integer  "recipient_id"
    t.string   "recipient_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "activities", ["owner_id", "owner_type"], name: "index_activities_on_owner_id_and_owner_type", using: :btree
  add_index "activities", ["recipient_id", "recipient_type"], name: "index_activities_on_recipient_id_and_recipient_type", using: :btree
  add_index "activities", ["trackable_id", "trackable_type"], name: "index_activities_on_trackable_id_and_trackable_type", using: :btree

  create_table "areas", force: true do |t|
    t.string   "area_type"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "c_contacts", force: true do |t|
    t.integer  "citizen_id"
    t.string   "contact_type"
    t.string   "location"
    t.string   "address_one"
    t.string   "address_two"
    t.string   "address_three"
    t.string   "city"
    t.string   "postcode"
    t.string   "telephone"
    t.string   "email"
    t.string   "website"
    t.boolean  "public"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "c_contacts", ["citizen_id"], name: "index_c_contacts_on_citizen_id", using: :btree

  create_table "c_infos", force: true do |t|
    t.integer  "citizen_id"
    t.string   "info_type"
    t.string   "info"
    t.boolean  "printout"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "c_infos", ["citizen_id"], name: "index_c_infos_on_citizen_id", using: :btree

  create_table "c_posts", force: true do |t|
    t.integer  "citizen_id"
    t.integer  "organisation_id"
    t.integer  "area_id"
    t.date     "start_date"
    t.date     "end_date"
    t.boolean  "current"
    t.string   "title"
    t.string   "role"
    t.string   "description"
    t.string   "post_type"
    t.boolean  "political"
    t.boolean  "printout"
    t.boolean  "keyrole"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "c_posts", ["area_id"], name: "index_c_posts_on_area_id", using: :btree
  add_index "c_posts", ["citizen_id"], name: "index_c_posts_on_citizen_id", using: :btree
  add_index "c_posts", ["organisation_id"], name: "index_c_posts_on_organisation_id", using: :btree

  create_table "c_profiles", force: true do |t|
    t.integer  "citizen_id"
    t.string   "profile_type"
    t.string   "profile"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "c_profiles", ["citizen_id"], name: "index_c_profiles_on_citizen_id", using: :btree

  create_table "citizens", force: true do |t|
    t.integer  "organisation_id"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "prefix"
    t.string   "suffix"
    t.string   "salutation"
    t.string   "notes"
    t.date     "dob"
    t.boolean  "extinct"
    t.string   "profile_title"
    t.string   "image",               limit: 50
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
  end

  add_index "citizens", ["organisation_id"], name: "index_citizens_on_organisation_id", using: :btree

  create_table "crm_assignments", force: true do |t|
    t.integer  "crm_issue_id"
    t.integer  "citizen_id"
    t.boolean  "noprevious"
    t.boolean  "printout"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "recent_determination"
    t.date     "recent_determination_date"
  end

  add_index "crm_assignments", ["citizen_id"], name: "index_crm_assignments_on_citizen_id", using: :btree
  add_index "crm_assignments", ["crm_issue_id"], name: "index_crm_assignments_on_crm_issues_id", using: :btree

  create_table "crm_determinations", force: true do |t|
    t.string   "determination"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "crm_issues", force: true do |t|
    t.integer  "organisation_id"
    t.string   "name"
    t.string   "status"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "crm_issues", ["organisation_id"], name: "index_crm_issues_on_organisation_id", using: :btree

  create_table "crm_meetings", force: true do |t|
    t.integer  "crm_assignment_id"
    t.date     "date"
    t.text     "notes",             limit: 2147483647
    t.integer  "determination_id"
    t.boolean  "printout"
    t.string   "event"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "crm_meetings", ["crm_assignment_id"], name: "index_crm_meetings_on_crm_assignment_id", using: :btree

  create_table "o_contacts", force: true do |t|
    t.integer  "organisation_id"
    t.string   "contact_type"
    t.string   "location"
    t.string   "address_one"
    t.string   "address_two"
    t.string   "address_three"
    t.string   "city"
    t.string   "postcode"
    t.string   "telephone"
    t.string   "email"
    t.string   "website"
    t.boolean  "public"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "o_contacts", ["organisation_id"], name: "index_o_contacts_on_organisation_id", using: :btree

  create_table "organisations", force: true do |t|
    t.string   "name"
    t.string   "short_name"
    t.string   "org_type"
    t.string   "abbreviation"
    t.boolean  "extinct"
    t.string   "notes"
    t.boolean  "client"
    t.date     "founded"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
